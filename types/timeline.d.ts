import Vue, { VNode } from 'vue';

export declare class Timeline extends Vue {
  /**
   * 时间轴方向，可选值为 horizontal（水平） 和 vertical（垂直）
   * @default vertical
   */
  mode: String;

  /**
   * 指定是否最后一个节点为幽灵节点
   * @default false
   */
  pending: boolean;
}


export declare class TimelineItem extends Vue {
  /**
   * 圆圈颜色，可选值为blue、red、green，或自定义色值
   * @default blue
   */
  color: String;

  'v-slots'?: {
    /**
     * 自定义时间轴点内容
     */
    dot?: () => any;

    /**
     * 基本内容
     */
    default?: () => any;
  };
}