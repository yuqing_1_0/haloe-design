import Vue from 'vue';

export declare class Button extends Vue {
  /**
   * 按钮类型，可选值为 default、primary、dashed、text、info、success、warning、error或者不设置
   * @default default
   */
  type?: '' | 'primary' | 'secondary' | 'error' | 'drop';
  /**
   * 幽灵属性，使按钮背景透明
   * @default false
   */
  ghost?: boolean;
  /**
   * 按钮大小，可选值为middle、small、default或者不设置
   * @default default
   */
  size?: '' | 'middle' | 'small' | 'default';
  /**
   * 按钮形状，可选值为circle或者不设置
   */
  shape?: '' | 'circle';
  /**
   * 开启后，按钮的长度为 100%
   * @default false
   */
  long?: boolean;
  /**
   * 设置button原生的type，可选值为button、submit、reset
   * @default button
   */
  'html-type'?: 'button' | 'submit' | 'reset';
  /**
   * 设置按钮为禁用状态
   * @default false
   */
  disabled?: boolean;
  /**
   * 设置按钮为加载中状态
   * @default false
   */
  loading?: boolean;
  /**
   * 设置按钮的图标类型
   */
  icon?: string;
  /**
   * 设置按钮的自定义图标
   */
  'custom-icon'?: string;
}

export declare class ButtonGroup extends Vue {
  /**
   * 按钮组合大小，可选值为middle、small、default或者不设置
   * @default default
   */
  size?: 'middle' | 'small' | 'default';
  /**
   * 按钮组合形状，可选值为circle或者不设置
   */
  shape?: '' | 'circle';
  /**
   * 是否纵向排列按钮组
   * @default false
   */
  vertical?: boolean;
}
