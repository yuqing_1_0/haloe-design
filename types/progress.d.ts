import Vue from 'vue';

export declare class Progress extends Vue {
  /**
   * progress类型，默认正常类型；small类型progress宽度为6px
   * @default 'default'
   */
  type?: 'default' | 'small';
  /**
   * progress的状态
   * @default 'normal'
   */
  status?: 'normal' | 'active' | 'wrong' | 'success';
  /**
   * progress百分比数值
   * @default 10
   */
  percent?: number;
  /** progress已完成部分百分比显示
   * @default 0
   */
  'suceess-percent'?: number;
  /**
   * progress宽度，单位px
   * @default 10
   */
  'stroke-width'?: number;
  /**
   * progress的颜色，可设置渐变色
   * @default ''
   */
  'stroke-color'?: string | any[];
  /**
   * progress是否垂直方向显示
   * @default false
   */
  'vertical'?: boolean;
  /**
   * 是否隐藏progress百分比数值或图标显示
   * @default false
   */
  'hide-info'?: boolean;
  /**
   * 百分比数值是否在progress内部显示
   * @default false
   */
  'text-inside'?: boolean;
  'v-slots'?: {
    /**
     * progress插槽
     */
    default?: () => any;

  };
}
