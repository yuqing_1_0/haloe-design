import { PluginFunction } from 'vue';

interface HaloE extends PluginFunction<any> {}

declare const HaloE: HaloE;

export default HaloE;

