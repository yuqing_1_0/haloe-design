import Vue, { VNode } from 'vue';

export declare class Tooltip extends Vue {
  /**
   * 提示内容
   */
  content?: string;
  /**
   * 提示使用浅色风格
   */
  light?: boolean;
  /**
   * 设置提示框宽度
   */
  width?: number;
  /**
   * 设置tag的颜色风格
   * @default 'top-left'
   */
  place?: string;
}
