import Vue from 'vue';
export declare class Breadcrumb extends Vue {
  /**
   * 自定义分隔符
   */
  separator?: string | Element;

  /**
   * 是否带有返回按钮
   * @default false
   */
  back?: boolean;

  /**
   * methods 点击返回按钮时触发此事件
   */
  backBtnClick?: (event?: any) => any;
}

export declare class BreadcrumbItem extends Vue {
  /**
   * 链接，不传则没有链接，支持 vue-router 对象
   */
  to?: string | object;

  /**
  * 路由跳转时，开启 replace 将不会向 history 添加新记录
  */
  replace?: boolean;

  /**
  * 相当于 a 链接的 target 属性
  */
  target?: '_blank' | '_self' | '_parent' | '_top';

  /**
  * 同 vue-router append
  */
  append?: boolean;
}
