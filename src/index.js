export * from './components';
import * as components from './components';
import localeFile from './locale/index';

// directives
import lineClamp from './directives/line-clamp';
import resize from './directives/resize';
import style from './directives/style';

// libraries
import dayjs from 'dayjs';

import pkg from '../package.json';

const directives = {
  display: style.display,
  width: style.width,
  height: style.height,
  margin: style.margin,
  padding: style.padding,
  font: style.font,
  color: style.color,
  'bg-color': style.bgColor,
  resize,
  'line-clamp': lineClamp
};



const Haloe = {
  ...components,
};

// console.log('HaloeUI components',HaloeUI) // eslint-disable-line no-console

export const install = function(app, opts = {}) {
  if (install.installed) return;
  if (opts.locale) {
    localeFile.use(opts.locale);
  }
  if (opts.i18n) {
    localeFile.i18n(opts.i18n);
  }

  Object.keys(Haloe).forEach(key => {
    app.component(`e${key}`, Haloe[key]);
  });

  Object.keys(directives).forEach(key => {
    app.directive(key, directives[key]);
  });

  app.config.globalProperties.$HALOE = {
    size: opts.size || '',
    capture: 'capture' in opts ? opts.capture : true,
    transfer: 'transfer' in opts ? opts.transfer : '',
    cell: {
      arrow: opts.cell ? opts.cell.arrow || '' : '',
      customArrow: opts.cell ? opts.cell.customArrow || '' : '',
      arrowSize: opts.cell ? opts.cell.arrowSize || '' : ''
    },
    menu: {
      arrow: opts.menu ? opts.menu.arrow || '' : '',
      customArrow: opts.menu ? opts.menu.customArrow || '' : '',
      arrowSize: opts.menu ? opts.menu.arrowSize || '' : ''
    },
    modal: {
      maskClosable: opts.modal ? 'maskClosable' in opts.modal ? opts.modal.maskClosable : '' : ''
    },
    tabs: {
      closeIcon: opts.tabs ? opts.tabs.closeIcon || '' : '',
      customCloseIcon: opts.tabs ? opts.tabs.customCloseIcon || '' : '',
      closeIconSize: opts.tabs ? opts.tabs.closeIconSize || '' : ''
    },
    select: {
      arrow: opts.select ? opts.select.arrow || '' : '',
      customArrow: opts.select ? opts.select.customArrow || '' : '',
      arrowSize: opts.select ? opts.select.arrowSize || '' : ''
    },
    colorPicker: {
      arrow: opts.colorPicker ? opts.colorPicker.arrow || '' : '',
      customArrow: opts.colorPicker ? opts.colorPicker.customArrow || '' : '',
      arrowSize: opts.colorPicker ? opts.colorPicker.arrowSize || '' : ''
    },
    cascader: {
      arrow: opts.cascader ? opts.cascader.arrow || '' : '',
      customArrow: opts.cascader ? opts.cascader.customArrow || '' : '',
      arrowSize: opts.cascader ? opts.cascader.arrowSize || '' : '',
      itemArrow: opts.cascader ? opts.cascader.itemArrow || '' : '',
      customItemArrow: opts.cascader ? opts.cascader.customItemArrow || '' : '',
      itemArrowSize: opts.cascader ? opts.cascader.itemArrowSize || '' : ''
    },
    tree: {
      arrow: opts.tree ? (opts.tree.arrow || '') : '',
      customArrow: opts.tree ? (opts.tree.customArrow || '') : '',
      arrowSize: opts.tree ? opts.tree.arrowSize || '' : ''
    },
    datePicker: {
      icon: opts.datePicker ? opts.datePicker.icon || '' : '',
      customIcon: opts.datePicker ? opts.datePicker.customIcon || '' : '',
      iconSize: opts.datePicker ? opts.datePicker.iconSize || '' : ''
    },
    timePicker: {
      icon: opts.timePicker ? opts.timePicker.icon || '' : '',
      customIcon: opts.timePicker ? opts.timePicker.customIcon || '' : '',
      iconSize: opts.timePicker ? opts.timePicker.iconSize || '' : ''
    }
  }

  app.config.globalProperties.$Date = dayjs;
  app.config.globalProperties.$Spin = components.Spin;
  app.config.globalProperties.$Notice = components.Notice;
  app.config.globalProperties.$Message = components.Message;
  app.config.globalProperties.$Modal = components.Modal;
};

export const version = pkg.version;

export const locale = localeFile.use;

export const i18n = localeFile.i18n;

export const lang = (code) => {
  const langObject = window['Haloe/locale'].default;
  if (code === langObject.i.locale) localeFile.use(langObject);
  else console.log(`The ${code} language pack is not loaded.`); // eslint-disable-line no-console
};

const API = {
  version,
  locale,
  i18n,
  install,
  lang,
  // Circle,
  // Switch,
  ...components
};

export default API;
