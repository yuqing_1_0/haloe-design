import { h } from 'vue';

export default {
  name: 'ExpandSlot',
  inject: ['TableInstance'],
  props: {
    row: Object,
    index: Number,
    column: {
      type: Object,
      default: null
    },
    display: {
      type: String,
      default: 'block'
    }
  },
  render () {
    return h('div', {
      'class': {
        'haloe-table-cell-slot': true,
        'haloe-table-cell-slot-inline': this.display === 'inline',
        'haloe-table-cell-slot-inline-block': this.display === 'inline-block'
      }
    }, this.TableInstance.$slots.expandSlot({
      row: this.row,
      column: this.column,
      index: this.index
    }));
  }
};
