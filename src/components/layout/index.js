import Layout from './layout.vue';
import Header from './header.vue';
import Sider from './sider.vue';
import Title from './title.vue';
import Content from './content.vue';
import Footer from './footer.vue'

Layout.Header = Header;
Layout.Sider = Sider;
Layout.Title = Title
Layout.Content = Content;
Layout.Footer = Footer

export default Layout;
