## 提示
### API
<br>

* **Tooltip Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|content|提示内容,若提示中要加icon可用v-slot:content|String|
|light|提示浅色风格|Boolean类型|false
|width|提示框宽度,以px为单位|Number||
|place|提示框弹出位置，可选top, top-left, top-right, left, left-top, left-bottom, bottom, bottom-left, bottom-right, right, right-top, right-bottom|String|top-left|
