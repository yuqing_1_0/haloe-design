
### API
<br>

* **Switch Props**

|属性    |说明    |类型    |   默认值   |
|-------|:------:|:------:|:-------:|
|value|指定当前是否选中，可以使用 v-model 双向绑定数据|Boolean|  false  |
|size|开关的尺寸，可选值为large、small、default或者不写。建议开关如果使用了2个汉字的文字，使用 large。|String| default |
|disabled|禁用开关|Boolean|  false  |
|true-value|选中时的值，当使用类似 1 和 0 来判断是否选中时会很有用|String, Number, Boolean|  true   |
|false-value|没有选中时的值，当使用类似 1 和 0 来判断是否选中时会很有用|String, Number, Boolean|  false  |
|true-color|自定义打开时的背景色|String|    -    |
|false-color|自定义关闭时的背景色|String|    -    |
|before-change|返回 Promise 可以阻止切换|Function|    -    |
|loading|加载中的开关|Boolean|  	false   |



* **Switch Events**

|事件名    |说明    |     返回值     |
|------- |:------:|:-----------:|
|on-change|开关变化时触发，返回当前的状态| true/false |

* **Switch Slots**

| 名称    |说明    |
|-------|:------:|
| open  |自定义显示打开时的内容|
| close |自定义显示关闭时的内容|

