
### API
<br>

* **Spin  Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|size|Spin尺寸，可选值为large和small、mini或者不设置|string|-|
|fix|是否固定，需要父级有relative或absolute|Boolean|false|
|disabled|是否禁用当前项	|Boolean|false|

* **Spin  events**

|事件名    |说明    |返回值    |
|------- |:------:|:------:|
|show|$Spin的全局函数，显示spin|无|
|hide|$Spin全局函数，移除spin|无|

* **Spin  slots**

|名称    |说明  |
|------- |:------:|
|无| 自定义 Spin 的内容，设置slot后，默认的样式不生效|
