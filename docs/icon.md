
### API
<br>

* **Icon Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|type|图标的名称|String|-|
|size|图标的大小，单位是 px|Number、String|-|
|color|图标的颜色|String|-|
|custom|自定义图标|String|-|


