
### API
<br>

* **searchPane Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|closeHeight|未展开时可见范围高度|String|'11rem'|
|expandHeight|展开后可见范围高度|String|'50rem'|
|type|组合筛选组件类型，可选值有simple，adaptive|String|'adaptive'|
|close-height|type为simple时有效，调整折叠时的div高度|String|'11rem'|
|minWidth|type为adaptive时有效，每个筛选条件的最小宽度|Nmuber|300|
|maxColumn|type为adaptive时有效，最大列数|Nmuber|5|


* **searchPane methods**

|事件名称    |说明    |回调参数    |
|------- |:------:|:------:|
|toSearch|	触发搜索事件|	void|
|toReset|	触发重置事件|	void|


