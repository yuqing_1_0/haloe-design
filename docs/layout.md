
### API
<br>

* **Sider  Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|model-value|是否收起, 可以使用 v-model来进行双向绑定|Boolean|false|
|width|宽度|Number|200|
|collapsible|是否可收起，设为false后，默认触发器会隐藏|Boolean|false|
|collapsed-width|收缩宽度|Number|64
|hide-trigger|隐藏默认触发器|Boolean|false
|default-collapsed|是否默认收起，设置了collapsible后设置此属性侧边栏仍会收起。|Boolean|false|



* **Sider  events**

|事件名    |说明    |返回值    |
|------- |:------:|:------:|
|on-collapse|展开-收起时的回调|true / false


* **Sider slot**

|名称    |说明    |
|------- |:------:|
|trigger|	自定义触发器|


* **Sider Methods**

|名称    |说明    |参数      |
|------- |:------:|:------:|
|toggleCollapse|改变Sider展开-收起状态。|       |


* **Layout slot**

|名称    |说明    |
|------- |:------:|
|default|		默认插槽内容|


* **Content Slots**

|名称    |说明    |
|------- |:------:|
|default|		默认插槽内容|


* **Footer Slots**

|名称    |说明    |
|------- |:------:|
|default|		默认插槽内容|


* **Header Slots**

|名称    |说明    |
|------- |:------:|
|default|		默认插槽内容|







