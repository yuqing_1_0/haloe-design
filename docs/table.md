
### API
<br>

* **Table Props**

| 属性             |                          说明                          |          类型          |   默认值   |
|----------------|:----------------------------------------------------:|:--------------------:|:-------:|
| data           |                       表格行结构化数据                       |        Array         |   []    |
| columns        |                       表格列配置数据                        |        Array         |   []    |
| size           |         表格尺寸，可选值为 `default`,`small`,`large`          |        String        | default |
| stripe         |                      是否设置斑马纹表格                       |       Boolean        |  false  |
| border         |                      是否显示表格纵向边框                      |       Boolean        |  false  |
| width          |                      表格宽度，单位px                       | Number &#124; String |   自动    |
| height         |                      表格高度，单位px                       | Number &#124; String |    -    |
| max-height     |              表格最大高度，如果表格大于此值会固定表头，单位px               | Number &#124; String |    -    |
| loading        |                      表格数据是否加载中                       |       Boolean        |  false  |
| show-header    |                       是否显示表格表头                       |       Boolean        |  true   |
| disabled-hover |                 鼠标悬停在表格上时，是否禁用鼠标悬停高亮                 |       Boolean        |  false  |
| no-data-text   |     表格数据为空时的提示内容，如果设置empty插槽内容，则`no-data-text`无效     |        String        |  暂无数据   |
| highlight-row  |             是否高亮显示当前选中的行，设置此属性后，可触发单选时间              |       Boolean        |  false  |
| row-class-name | 行的 `className` 的回调方法，传入参数：`row`：当前行数据 `index`：当前行的索引 |       Function       |    -    |
| fixed-shadow   |           列固定时，阴影显示规则，可选值为 `auto`、`show`、`hide`            |        String        |  show   |



* **Table Events**


| 事件                  |                  说明                   |                               返回值                                |
|---------------------|:-------------------------------------:|:----------------------------------------------------------------:|
| on-current-change   | 设置`highlight-row`属性后，当选择表格的当前行发生变化时触发 |          `currentRow`:当前选择行的数据，`oldCurrentRow`:之前选择行的数据          |
| on-select           |            多选模式下有效，选中某项时触发            |                 `selection`:已选项数据，`row`:当前选择项的数据                 |
| on-select-cancel    |           多选模式下有效，取消选中某项时触发           |                 `selection`:已选项数据，`row`:当前被取消的数据                 |
| on-select-all       |            多选模式下有效，点击全选时触发            |                        `selection`:已选项数据                         |
| on-selection-change |          多选模式下有效，选中项发生变化时触发           |                        `selection`:已选项数据                         |
| on-sort-change      |             排序时有效，点击排序时触发             | `column`:当前列数据，`key`:排序依据的指标，`order`:排序的顺序，为`asc（升序）`或`desc（降序）` |
| on-row-click        |              单击单表格某行时触发               |                    `row`:当前行数据，`index`:当前行索引                     |
| on-row-dblclick     |               双击表格某行时触发               |                    `row`:当前行的数据，`index`:当前行索引                    |
| on-cell-click       |              单击表格单元格时触发               |                  `row`，`column`，`data`，`event`                   |
| on-expand           |              展开/收起某行时触发               |                   `row`:当前行的数据，`status`:当前的状态                    |

* **Table Methods**


| 方法名             |              说明              |   参数   |
|-----------------|:----------------------------:|:------:|
| clearCurrentRow | 清除高亮项，仅在开启`highlight-row`时可用 |   无    |
| selectAll       |        多选模式下，全选/取消全选         | status |


* **Table Slots**

| 插槽名 |                  说明                   |
|--|:-------------------------------------:|
| empty | 表格数据为空时使用的插槽，设置插槽内容后，`no-data-text`无效 |
| header |            表头插槽，自定义表头内容时使用            |
| loading |                数据加载中插槽                |
| expandSlot |                展开插槽，其他自定义插槽不允许重名                |

* **Column Props**

| 属性           |                                                说明                                                |           类型            |  默认值  |
|--------------|:------------------------------------------------------------------------------------------------:|:-----------------------:|:-----:|
| type         |                            列类型，可选`index`、`selection`、`expand`、`html`                             |         String          |   -   |
| title        |                                              列头显示文字                                              |         String          |   #   |
| key          |                                            对应列内容的字段名                                             |         String          |   -   |
| width        |                                                列宽                                                |         Number          |   -   |
| minWidth     |                                               最小列宽                                               |         Number          |   -   |
| maxWidth     |                                               最大列宽                                               |         Number          |   -   |
| align        |                                 对齐方式，可选值`left`、`right`、`center`                                  |         String          | left  |
| className    |                                              列样式名称                                               |         String          |   -   |
| fixed        |                                     列是否固定，可选值`left`、`right`                                      |         String          |   -   |
| ellipsis     |                                        设置后，文本超出表格列时显示省略号                                         |         Boolean         | false |
| render       | 自定义渲染列，使用 Vue 的 Render 函数。传入两个参数，第一个是 h，第二个为对象，包含 `row`、`column` 和 `index`，分别指当前行数据，当前列数据，当前行索引。 |        Function         |   -   |
| renderHeader |     自定义列头显示内容，使用 Vue 的 Render 函数。传入两个参数，第一个是 h，第二个为对象，包含 `column` 和 `index`，分别为当前列数据和当前列索引。      |        Function         |   -   |
| sortable     |              对应列是否可以排序，如果设置为 `custom`，则代表用户希望远程排序，需要监听 Table 的 `on-sort-change` 事件               | Boolean &#124; `custom` | false |
| sortMethod   |         自定义排序使用的方法，接收三个参数 a 、 b 和 type，当设置 `sortable`: `true` 时有效。type 值为 `asc` 和 `desc`         |        Function         |   -   |
| sortType     |                                    设置初始化排序。值为 `asc` 和 `desc`                                     |         String          |   -   |
| resizable    |                                     该列是否允许拖拽调整列宽                                    |         Boolean         | false |
