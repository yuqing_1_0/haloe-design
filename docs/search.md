### API
<br/>

* **Search Props**

|属性    |说明    |类型    |默认值    |
|------- |:------:|:------:|--------:|
|is-enter|区分是回车键搜索样式还是按钮搜索样式， 默认是按钮搜索样式|Boolean|false|
|placeholder|默认文字|String|''|
|filter-items|过滤条件的选项，传入此值启用过滤条件样式|Array|null|
|filterable|是否支持搜索|Boolean|false|
|filter-width|过滤条件选择器的宽度|Number,String|90|
|clearable|是否显示清空按钮|Boolean|true|
|filter-value|筛选条件默认值|any|null|


* **Search Event**

|方法名|说明|参数|
|------- |:------:|--------:|
|on-submit|点击搜索图标触发|第一个参数是搜索框的文本，第二个参数是过滤条件，当传入filter-items时第二个参数有效|
|on-select|筛选条件输入框切换时触发	|第一个参数是过滤条件|
|on-enter|按下回车按钮触发|第一个参数是搜索框的文本，第二个参数是过滤条件，当传入filter-items时第二个参数有效|

