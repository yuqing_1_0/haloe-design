## 气泡确认框
### API
<br>

* **Popconfirm Props**

| 属性             |                                                      说明                                                       |          类型          |    默认值    |
|----------------|:-------------------------------------------------------------------------------------------------------------:|:--------------------:|:---------:|
| placement      | 显示位置，可选位置`top-start`,`top`,`top-end`,`left-start`,`left`, `left-end`,<br> `right-start`, `right`, `right-end` |        String        |    top    |
| trigger        |                                      触发方式，可选触发方式`click`,`hover`,`focus`                                       |        String        |   click   |
| title          |                                                     标题内容                                                      |        String        |     -     |
| content        |                                                     主体内容                                                      |        String        |     -     |
| width          |                                               popconfirm显示内容宽度                                                | String &#124; Number |     -     |
| visible-arrow  |                                              是否显示popconfirm指示箭头                                               |       Boolean        |   true    |
| confirm        |                                                  是否开启气泡确认框模式                                                  |       Boolean        |   false   |
| disabled       |                                                     是否禁用                                                      |       Boolean        |   false   |
| ok-text        |                                           确定按钮的文字，只在 confirm 模式下有效                                            |        String        |    确定     |
| cancel-text    |                                           取消按钮的文字，只在 confirm 模式下有效                                            |        String        |    取消     |
| word-wrap      |                                            开启后，超出指定宽度文本将自动换行，并两端对齐                                            |       Boolean        |   false   |
| padding        |                                                    自定义间距值                                                     |        String        | 8px, 16px |
| offset         |                                                   出现位置的偏移值                                                    |        Number        |     0     |
| popper-class   |                                          给 popconfirm 设置 class-name                                           |        String        |     -     |
| events-enabled |                                        是否开启 Popper 的 eventsEnabled 属性                                         |       Boolean        |   false   |
| options        |           自定义 popper.js 的配置项，具体配置见 [popper.js 文档](https://popper.js.org/docs/v1/){target:"_blank"}            |        Object        |     -     |
| transfer       |                  是否将弹层放置于 body 内，在 Tabs、带有 fixed 的 Table 列内使用时，建议添加此属性，它将不受父级样式影响，从而达到更好的效果                   |       Boolean        |   false   |
| transfer-class-name        |                                        开启 transfer 时，给浮层添加额外的 class 名称                                        |        String        |     -     |

* **Popconfirm Events**

| 事件             |              说明               |
|----------------|:-----------------------------:|
| on-ok          | popconfirm为气泡确认框模式时，`确认`操作时触发 |
| on-cancel      | popconfirm为气泡确认框模式时，`取消`操作时触发 |
| on-popper-show |           popconfirm提示框显示时触发            |
| on-popper-hide |           popconfirm提示框消失时触发            |

* **Popconfirm Slots**

| 插槽名     |       说明        |
|---------|:---------------:|
| title   | 标题插槽，通过该插槽可自定义`title`内容 |
| content | 默认插槽，通过该插槽可自定义主体内容 |
