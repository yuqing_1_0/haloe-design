
### API

* **Tree Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|data	|可嵌套的节点属性的数组，生成 tree 的数据	|Array	|[]|
|multiple	|是否支持多选	|Boolean	|false|
|show-checkbox|	是否显示多选框|	Boolean	|false|
|empty-text	|没有数据时的提示	|String|	暂无数据|
|load-data	|异步加载数据的方法，见示例	|Function|	-|
|render	|自定义渲染内容，见示例	|Function	|-|
|children-key	|定义子节点键|	String	|children|
|check-strictly	|在显示复选框的情况下，是否严格的遵循父子不互相关联的做法|	Boolean|false|
|check-directly	|开启后，在 show-checkbox 模式下，select 的交互也将转为 check|	Boolean	|false|
|select-node	|开启后，点击节点将使用单选效果|	Boolean|	true|
|expand-node|	开启后，点击节点将使用展开/收起子节点效果，该选项优先于 select-node|Boolean|false|
|auto-close-contextmenu	|点击右键菜单项是否自动关闭右键菜单|Boolean	|true|


* **Tree Events**

|事件名    |说明    |返回值    |
|------- |:------:|:------:|
|on-select-change|	点击树节点时触发|	当前已选中的节点数组、当前项|
|on-check-change	|点击复选框时触发	|当前已勾选节点的数组、当前项|
|on-toggle-expand|	展开和收起子列表时触发|	当前节点的数据|
|on-contextmenu|	当前节点点击右键时触发	|data, event, position|

* **Tree Slots**

|属性    |说明    |
|------- |:------:|:------:|--------:|
|contextMenu|	右键菜单，详见示例|

* **Tree Methods**

|方法名|	说明|	参数|
|------- |:------:|:------:|--------:|
|getCheckedNodes|	获取被勾选的节点|	无|
|getSelectedNodes	|获取被选中的节点|	无|
|getCheckedAndIndeterminateNodes|	获取选中及半选节点|	无|

* **Children**

|属性|	说明|	类型	|默认值|
|------- |:------:|:------:|--------:|
|title|	标题	|String \ Element String|	-|
|expand	|是否展开直子节点	|Boolean	|false|
|disabled|	禁掉响应|	Boolean|	false|
|disableCheckbox|	禁掉 checkbox|	Boolean	|false|
|selected|	是否选中子节点|	Boolean	|false|
|checked|	是否勾选(如果勾选，子节点也会全部勾选)|	Boolean	|false|
|children|	子节点属性数组	|Array|	-|
|render|	自定义当前节点渲染内容，见示例|	Function|	-|
|contextmenu|	是否支持右键菜单	|Boolean	|false|



