### API
<br>

* **Upload  Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|action|上传的地址，必填|String |-|
|headers|设置上传的请求头部|Object|{}|
|multiple|是否支持多选文件|Boolean|false|
|data|上传时附带的额外参数	|Object|{}|
|name|上传的文件字段名|String|file|
|with-credentials|支持发送 cookie 凭证信息|Boolean|false|
|show-upload-list|是否显示已上传文件列表|Boolean|true|
|type|上传控件的类型，可选值为 select（点击选择），drag（支持拖拽），drag-1（支持拖拽）|String|select|
|format|支持的文件类型，与 accept 不同的是，format 是识别文件的后缀名，accept 为 input 标签原生的 accept 属性，会在选择文件时过滤，可以两者结合使用 |Array|[]|
|accept|接受上传的文件类型|String|-|
|max-size|文件大小限制，单位 kb|Number|-|
|default-file-list|默认已上传的文件列表，例如：[{name: 'img1.jpg', url: 'http://www.xxx.com/img1.jpg'}, {name: 'img2.jpg', url: 'http://www.xxx.com/img2.jpg'}]|Array|[]|
|paste|是否支持粘贴上传文件|Boolean|false|
|disabled|是否禁用|Boolean|false|
|webkitdirectory|是否开启选择文件夹，部分浏览器适用|Boolean|false|

* **Upload Events**

|事件名    |说明    |返回值   |
|-------|:------:|:------:|
|before-upload|上传文件之前的钩子，参数为上传的文件，若返回 false 或者 Promise 则停止上传|-|
|on-progress|文件上传时的钩子|event, file, fileList|
|on-success|文件上传成功时的钩子|response, file, fileList|
|on-error|文件上传失败时的钩子|error, file, fileList|
|on-preview|点击已上传的文件链接时的钩子|file|
|on-remove|文件列表移除文件时的钩子|file, fileList|
|on-format-error|文件格式验证失败时的钩子|file, fileList|
|on-exceeded-size|文件超出指定大小限制时的钩子|file, fileList|


* **Upload Slots**

|名称    |说明    |
|-------|:------:|
|tip|辅助提示内容|
|fileDetails|在drag-1模式下可自定义额外的显示内容|
