
### API
<br>

* **Button Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|type|按钮类型，可选值为 'primary', 'secondary', 'error', 'drop'或者不设置|String|secondary|
|size|按钮大小，可选值为 default、middle、small 或者不设置|Boolean|default|
|disabled|是否禁用当前项|Boolean|false|
|loading|设置按钮为加载中状态|Boolean|false|
|html-type|设置 button 原生的 type，可选值为 button、submit、reset|String|button|
|icon|设置按钮的图标类型|String|-|
|custom-icon|设置按钮的自定义图标|String|-|
|ghost|反白属性，反白按钮将其他按钮的内容反色，背景变为透明，常用在有色背景上。|Boolean|false|



* **Button Events**

|事件名    |说明    |返回值    |
|------- |:------:|:------:|
|click|点击时触发|event|

