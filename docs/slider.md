## 滑动输入框
### API
<br>

* **Slider Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|model-value|滑块选定的值，可以使用 v-model 双向绑定数据。普通模式下，数据格式为数字，在双滑块模式下，数据格式为长度是2的数组，且每项都为数字|Number &#124; Array|0|
|min|最小值|Number|0|
|max|最大值|Number|100|
|step|步长，取值建议能被（max - min）整除|Number|1|
|disabled|是否禁用滑块|Boolean|false|
|range|是否开启双滑块模式|	Boolean|false|
|show-input	|是否显示数字输入框，仅在单滑块模式下有效|	Boolean|false|
|tip-format|Slider 会把当前值传给`tip-format`，并在 Tooltip 中显示 tip-format 的返回值，若为 null，则隐藏 Tooltip|Function|value|
|input-size|数字输入框的尺寸，可选值为`large`、`small`、`default`或者不填，仅在开启 show-input 时有效|	String|default|
|active-change|同 InputNumber 的 active-change|	Boolean|true|
|marks|标记， key 的类型必须为 number 且取值在闭区间 [min, max] 内，每个标记可以单独设置样式	|Object|-|


* **Slider Events**

|事件名    |说明    |返回值    |
|------- |:------:|:------:|
|on-change|在松开滑动时触发，返回当前的选值，在滑动过程中不会触发|value|
|on-input|滑动条数据变化时触发，返回当前的选值，在滑动过程中实时触发|value|
