import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import App from './app.vue';
import Haloe from '../src/index';
import mitt from "mitt"
import eventBus from 'vue3-eventbus'
// import locale from '../src/locale/lang/en-US';
// import locale from '../src/locale/lang/zh-CN';
// markdown样式
import 'github-markdown-css'
// 代码高亮
import 'highlight.js/styles/github.css'

// 路由配置
const router = createRouter({
  esModule: false,
  mode: 'history',
  history: createWebHistory(),
  routes: [
    {
      path: '/radio',
      component: () => import('./routers/radio.vue'),
    },
    {
      path: '/input-number',
      component: () => import('./routers/input-number.vue'),
    },
    {
      path: '/input',
      component: () => import('./routers/input.vue'),
    },
    {
      path: '/auto-complete',
      component: () => import('./routers/auto-complete.vue'),
    },
    {
      path: '/button',
      component: () => import('./routers/button.vue'),
    },
    {
      path: '/icon',
      component: () => import('./routers/icon.vue'),
    },
    {
      path: '/cascader',
      component: () => import('./routers/cascader.vue')
    },
    {
      path: '/checkbox',
      component: () => import('./routers/checkbox.vue'),
    },
    {
      path: '/select',
      component: () => import('./routers/select'),
    },
    {
      path: '/tree-select',
      component: () => import('./routers/tree-select')
    },
    {
      path: '/tree',
      component: () => import('./routers/tree')
    },
    {
      path: '/search-pane',
      component: () => import('./routers/search-pane')
    },
    {
      path: '/tabs',
      component: () => import('./routers/tabs.vue'),
    },
    {
      path: '/switch',
      component: () => import('./routers/switch.vue'),
    },
    {
      path: '/collapse',
      component: () => import('./routers/collapse.vue'),
    },
    {
      path: '/badge',
      component: () => import('./routers/badge.vue'),
    },
    {
      path: '/tag',
      component: () => import('./routers/tag.vue'),
    },
    {
      path: '/progress',
      component: () => import('./routers/progress'),
    },
    {
      path: '/spin',
      component: () => import('./routers/spin.vue'),
    },
    {
      path: '/page',
      component: () => import('./routers/page.vue'),
    },
    {
      path:'/page',
      component:()=>import('./routers/page.vue')
    },
    {
      path: '/tooltip',
      component: () => import('./routers/tooltip.vue')
    },
    {
      path: '/modal',
      component: () => import('./routers/modal.vue')
    },
    {
      path: '/breadcrumb',
      component: () => import('./routers/breadcrumb.vue')
    },
    {
      path: '/layout',
      component: () => import('./routers/layout.vue'),
    },
    ,
    {
      path: '/notice',
      component: () => import('./routers/notice.vue')
    },
    {
      path: '/message',
      component: () => import('./routers/message.vue')
    },
    {
      path: '/search',
      component: () => import('./routers/search.vue'),
    },
    {
      path: '/date-picker',
      component: () => import('./routers/date-picker.vue'),
    },
    {
      path: '/slider',
      component: () => import('./routers/slider.vue'),
    },
    {
      path: '/table',
      component: () => import('./routers/table.vue'),
    },
    {
      path: '/popconfirm',
      component: () => import('./routers/popconfirm.vue')
    },
    {
      path: '/transfer',
      component: () => import('./routers/transfer.vue'),
    },
    {
      path: '/message',
      component: () => import('./routers/message.vue')
    },
    {
      path: '/menu',
      component: () => import('./routers/menu.vue')
    },
    {
      path: '/steps',
      component: () => import('./routers/steps.vue')
    },
    {
      path: '/alert',
      component: () => import('./routers/alert.vue')
    },
    {
      path: '/timeline',
      component: () => import('./routers/timeline.vue')
    },
    {
      path: '/drawer',
      component: () => import('./routers/drawer.vue')
    },
    {
      path: '/upload',
      component: () => import('./routers/upload/index.vue')
    },
    {
      path: '/time-picker',
      component: () => import('./routers/time-picker.vue')
    }
  ]
});

const app = createApp(App);

app.config.globalProperties.$eInput = Haloe.Input

app.use(Haloe);
app.use(router);
app.use(eventBus)
app.mount('#app');
app.config.globalProperties.$bus = new mitt();

export default app
